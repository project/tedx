<?php

/**
 *
 * Markup for Acquia Marina rounded corners.
 *
 */
?>
<div class="tedx-corner-wrapper">
  <div class="tedx-corner-corner">
    <div class="tedx-corner-top-left"></div>
    <div class="tedx-corner-top-right"></div>
    <div class="tedx-corner-outside">
      <div class="tedx-corner-inside">
        <p class="tedx-corner-topspace"></p>
          <?php print $content; ?>
        <p class="tedx-corner-bottomspace"></p>
      </div><!-- /tedx-corner-inside -->
    </div>
    <div class="tedx-corner-bottom-left"></div>
    <div class="tedx-corner-bottom-right"></div>
  </div><!-- /tedx-corner -->
</div><!-- /tedx-corner-wrapper -->
