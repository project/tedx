
This is a theme for sites serving as support for TEDx events.

It is based on Acquia Marina.

A detailed account on the adaptation of acquia_marina and its transformation 
to look and feel like TED is posted on:

http://www.socinfo.com/tedx/theme

There are additional images (logo, favicon.ico) with the TEDx logo, which
can't be included in the theme because of the GPS licensing scheme.

This theme was originally implemented to support TEDxSantoDomingo 2009 and 
can be seen at use at: http://www.tedxsantodomingo.com

It was revised and updated for the 2010 TEDxSantoDomingo event.

TED is a nonprofit organization devoted to Ideas Worth Spreading. Started as a four-day conference in California 25 years ago, TED has grown to support those world-changing ideas with multiple initiatives.

In the spirit of ideas worth spreading, TED has created a program called TEDx. TEDx is a program of local, self-organized events that bring people together to share a TED-like experience.

Our event is called TEDxSantoDomingo, where x = independently organized TED event. At our TEDxSantoDomingo event, TEDTalks video and live speakers will combine to spark deep discussion and connection in a small group.

The TED Conference provides general guidance for the TEDx program, but individual TEDx events, including ours, are self-organized.

For more info, or questions, contact Carlos Miranda Levy - carlos@socinfo.com